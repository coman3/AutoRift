﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EloBuddy;
using EloBuddy.SDK;
using EloBuddy.SDK.Events;

namespace AutoRift
{
    class Program
    {
        static void Main()
        {
            Loading.OnLoadingComplete += Loading_OnLoadingComplete;
        }

        private static void Loading_OnLoadingComplete(EventArgs args)
        {
            if (!ChampionController.LoadChampion(Player.Instance))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Champion " + Player.Instance.ChampionName + " not Supported, failed To load!");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }
            //Champion has been loaded! Lets prepare everything
            //Drawing
            Drawing.OnBeginScene += (e) => ChampionController.ChampionPlugin.OnBeginDraw();
            Drawing.OnDraw += (e) => ChampionController.ChampionPlugin.OnDraw();
            Drawing.OnEndScene += (e) => ChampionController.ChampionPlugin.OnEndDraw();
            //Game Events
            Game.OnUpdate += (e) => ChampionController.ChampionPlugin.OnGameUpdate();
            Game.OnEnd += ChampionController.ChampionPlugin.OnEnd;

            //Player
            Player.OnIssueOrder += ChampionController.ChampionPlugin.OnIssueOrder;
            AIHeroClient.OnSpawn += ChampionController.ChampionPlugin.OnSpawn;
            Obj_AI_Base.OnLevelUp += ChampionController.ChampionPlugin.OnLevelUp;
            Obj_AI_Base.OnBuffGain += ChampionController.ChampionPlugin.OnBuffGain;
            Obj_AI_Base.OnBuffLose += ChampionController.ChampionPlugin.OnBuffLose;
            AttackableUnit.OnDamage += ChampionController.ChampionPlugin.OnDamage;
            AIHeroClient.OnDeath += ChampionController.ChampionPlugin.OnDeath;
            Obj_AI_Base.OnSurrender += ChampionController.ChampionPlugin.OnSurrender;

        }
    }
}
