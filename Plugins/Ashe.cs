﻿using AutoRift.Core;
using EloBuddy;

namespace AutoRift.Plugins
{
    [ChampionPlugin(ChampionName = "Ashe", SuportedVersion = "1.0.0.0")]
    public class Ashe : ChampionPlugin
    {
        public override void OnGameUpdate()
        {
            
        }

        public override void OnIssueOrder(Obj_AI_Base sender, PlayerIssueOrderEventArgs eventArgs)
        {
            
        }

        public override void OnSpawn(Obj_AI_Base sender)
        {
            
        }

        public override void OnLevelUp(Obj_AI_Base sender, Obj_AI_BaseLevelUpEventArgs args)
        {
            
        }

        public override void OnDamage(AttackableUnit sender, AttackableUnitDamageEventArgs args)
        {
            
        }

        public override void OnDeath(Obj_AI_Base sender, OnHeroDeathEventArgs args)
        {
            
        }

        public override void OnDraw()
        {
            
        }

        public override void OnEnd(GameEndEventArgs args)
        {

        }
    }
}