﻿using System;
using EloBuddy;

namespace AutoRift.Core
{
    /// <summary>
    /// Base class for every champion plugin
    /// </summary>
    public abstract class ChampionPlugin
    {
        //Game Events
        public abstract void OnGameUpdate();
        public abstract void OnEnd(GameEndEventArgs args);

        //Player Events
        public abstract void OnIssueOrder(Obj_AI_Base sender, PlayerIssueOrderEventArgs eventArgs);
        public abstract void OnSpawn(Obj_AI_Base sender);
        public abstract void OnLevelUp(Obj_AI_Base sender, Obj_AI_BaseLevelUpEventArgs args);
        public abstract void OnDamage(AttackableUnit sender, AttackableUnitDamageEventArgs args);
        public abstract void OnDeath(Obj_AI_Base sender, OnHeroDeathEventArgs args);
        public virtual void OnSurrender(Obj_AI_Base sender, Obj_AI_BaseSurrenderVoteEventArgs args) { }
        public virtual void OnBuffGain(Obj_AI_Base sender, Obj_AI_BaseBuffGainEventArgs args) { }
        public virtual void OnBuffLose(Obj_AI_Base sender, Obj_AI_BaseBuffLoseEventArgs args) { }

        //Drawing
        public virtual void OnBeginDraw() { }
        public abstract void OnDraw();
        public virtual void OnEndDraw() { }


    }

    public class ChampionPluginAttribute : Attribute
    {
        public string ChampionName { get; set; }
        public string SuportedVersion { get; set; }

    }
}