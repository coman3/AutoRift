﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AutoRift.Core
{
    public static class Helpers
    {
        public static TItem RandomItem<TItem>(this IEnumerable<TItem> enumerable)
        {
            var list = enumerable.ToList();
            if (list.Count == 1)
                return list[0];
            var random = new Random(Guid.NewGuid().GetHashCode());
            return list[random.Next(0, list.Count - 1)];
        }
    }
}