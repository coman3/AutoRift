﻿using System.Collections.Generic;
using System.Linq;
using AutoRift.Core;
using EloBuddy;
using EloBuddy.SDK;
using SharpDX;

namespace AutoRift.Situation
{
    public static class Ally
    {
        public static List<AIHeroClient> Heroes { get { return EntityManager.Heroes.Enemies; } }
        public static Dictionary<AIHeroClient, SpellAvaliblity> SpellAvaliblities = new Dictionary<AIHeroClient, SpellAvaliblity>();

        static Ally()
        {
            foreach (var ally in Heroes)
            {
                SpellAvaliblities.Add(ally, new SpellAvaliblity(ally));
            }
        } 
    }
}